﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sim.ej27.proyecto
{
    public class Iteracion
    {
        public long numero { get; set; }
        public string evento { get; set; }
        public double reloj { get; set; }
        public double rndLlegadaReloj { get; set; }
        public double tiempoEntreLlegadaReloj { get; set; }
        public double proximaLlegadaReloj { get; set; }
        public List<Reloj> cola { get; set; }
        public double rndFinControl { get; set; }
        public double tiempoControl { get; set; }
        public double tiempoFinControl { get; set; }
        public double tiempoFinControl2 { get; set; }
        public double tiempoFinControl3 { get; set; }
        public List<Empleado> Empleados { get; set; }
        public int cantRelojLlegan { get; set; }
        public int cantRelojesAceptados { get; set; }
        public int cantRelojesConEsperaFinalizada { get; set; }
        public int cantRelojesControlados { get; set; }
        public double acuTiempoEsperaReloj { get; set; }
        public double acumTiempoInicioEmpleadoOcupado { get; set; }
        public double acumTiempoControlTotalReloj { get; set; }
        public List<Reloj> Relojes { get; set; }
        
        public Iteracion(List<Empleado> empleados) {
            this.Empleados = empleados;
            this.Relojes = new List<Reloj>();
            this.numero = 0;
            this.cola = null;
        }
        
        public Iteracion(long numero, Iteracion it) {
            this.proximaLlegadaReloj = it.proximaLlegadaReloj;
            this.Relojes = it.Relojes;
            this.Empleados = it.Empleados;
            this.numero = numero;
            this.cantRelojLlegan = it.cantRelojLlegan;
            this.cantRelojesAceptados = it.cantRelojesAceptados;
            this.cantRelojesConEsperaFinalizada = it.cantRelojesConEsperaFinalizada;
            this.cantRelojesControlados = it.cantRelojesControlados;
            this.acuTiempoEsperaReloj = it.acuTiempoEsperaReloj;
            this.acumTiempoInicioEmpleadoOcupado = it.acumTiempoInicioEmpleadoOcupado;
            this.cola = it.cola;
            this.acumTiempoControlTotalReloj = it.acumTiempoControlTotalReloj;
        }
       
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public void generarFinAtencion(Iteracion filaActual,long empleadoId, Double valorDesviacion, Double valorMedia)
        {
            double rnd = Form1.nextRND();
            double tiempoAtencion = (rnd * valorDesviacion + valorMedia);
            filaActual.rndFinControl = Math.Round(rnd, 2);
            filaActual.tiempoControl = Math.Round(tiempoAtencion, 2);
            this.tiempoFinControl = Math.Round(filaActual.reloj + filaActual.tiempoControl, 2);
            Empleado empleado=filaActual.Empleados.Find(item => item.id == empleadoId);
        }

        public Empleado obtenerProximoEventoFinAtencion1(Iteracion fila)
        {
            Empleado empleado = null;
            List<Empleado> empleadosOcupados = Empleados.FindAll(item => item.estado.Equals(Empleado.ESTADO_OCUPADO));
            for (int i = 0; i < empleadosOcupados.Count; i++)
            {
                if (i == 0)
                {
                    empleado = empleadosOcupados.ElementAt(i);
                    
                }
                else if (empleadosOcupados.ElementAt(i).horaFinAtencion != 0
                  && empleadosOcupados.ElementAt(i).horaFinAtencion < empleado.horaFinAtencion)
                {
                    empleado = empleadosOcupados.ElementAt(i);
                }
            }
            return empleado;
        }

        public Empleado obtenerProximoEventoFinAtencion()
        {
            Empleado empleado = null;
            List<Empleado> empleadosOcupados = Empleados.FindAll(item => item.estado.Equals(Empleado.ESTADO_OCUPADO));
            for (int i = 0; i < empleadosOcupados.Count; i++)
            {
                if (i == 0)
                {
                    empleado = empleadosOcupados.ElementAt(i);
                }
                else if (empleadosOcupados.ElementAt(i).horaFinAtencion != 0
                    && empleadosOcupados.ElementAt(i).horaFinAtencion < empleado.horaFinAtencion)
                {
                    empleado = empleadosOcupados.ElementAt(i);
                }
            }
            return empleado;
        }
        
        public Reloj extraerPrimeroCola(Iteracion filaActual)
        {
            Reloj reloj = cola.First();
            this.cola.RemoveAt(0);
            this.cola.TrimExcess();
            return reloj;
        }
    }
}
