﻿namespace sim.ej27.proyecto
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BtnSimular = new System.Windows.Forms.Button();
            this.groupParamentrosEntrada = new System.Windows.Forms.GroupBox();
            this.txtIteraciones = new System.Windows.Forms.TextBox();
            this.lblIteraciones = new System.Windows.Forms.Label();
            this.txtMostrarHasta = new System.Windows.Forms.TextBox();
            this.lblMostrarHasta = new System.Windows.Forms.Label();
            this.txtMostrarDesde = new System.Windows.Forms.TextBox();
            this.lblMostrarDesde = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupSalida = new System.Windows.Forms.GroupBox();
            this.txtResultados = new System.Windows.Forms.TextBox();
            this.gridView = new System.Windows.Forms.DataGridView();
            this.txtMediaLlegadaReloj = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtControlDesviacion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtControlMedia = new System.Windows.Forms.TextBox();
            this.btnEnunciado = new System.Windows.Forms.Button();
            this.colIteracion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReloj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEvento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRndLlegadaReloj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTiempoEntreLlegadaReloj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProximaLlegadaReloj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRndFinControl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTControl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTiempoAtencion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTiempoAtencion2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTiempoAtencion3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCola = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Empleado_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmpleado1HoraInicioOcup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumEmpleado1Ocupacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Empleado_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmpleado2HoraInicioOcup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumEmpleado2Ocupacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Empleado_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmpleado3HoraInicioOcup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acumEmpleado3Ocupacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantClientesLlegan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantRelojesControlados = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantClientesConEsperaFinalizada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantClientesAceptados = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acuTiempoEsperaReloj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupParamentrosEntrada.SuspendLayout();
            this.groupSalida.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnSimular
            // 
            this.BtnSimular.Location = new System.Drawing.Point(27, 123);
            this.BtnSimular.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSimular.Name = "BtnSimular";
            this.BtnSimular.Size = new System.Drawing.Size(162, 50);
            this.BtnSimular.TabIndex = 0;
            this.BtnSimular.Text = "Simular";
            this.BtnSimular.UseVisualStyleBackColor = true;
            this.BtnSimular.Click += new System.EventHandler(this.BtnSimular_Click);
            // 
            // groupParamentrosEntrada
            // 
            this.groupParamentrosEntrada.Controls.Add(this.txtIteraciones);
            this.groupParamentrosEntrada.Controls.Add(this.lblIteraciones);
            this.groupParamentrosEntrada.Controls.Add(this.txtMostrarHasta);
            this.groupParamentrosEntrada.Controls.Add(this.lblMostrarHasta);
            this.groupParamentrosEntrada.Controls.Add(this.txtMostrarDesde);
            this.groupParamentrosEntrada.Controls.Add(this.BtnSimular);
            this.groupParamentrosEntrada.Controls.Add(this.lblMostrarDesde);
            this.groupParamentrosEntrada.Location = new System.Drawing.Point(250, 26);
            this.groupParamentrosEntrada.Margin = new System.Windows.Forms.Padding(4);
            this.groupParamentrosEntrada.Name = "groupParamentrosEntrada";
            this.groupParamentrosEntrada.Padding = new System.Windows.Forms.Padding(4);
            this.groupParamentrosEntrada.Size = new System.Drawing.Size(208, 181);
            this.groupParamentrosEntrada.TabIndex = 2;
            this.groupParamentrosEntrada.TabStop = false;
            this.groupParamentrosEntrada.Text = "Simulación";
            // 
            // txtIteraciones
            // 
            this.txtIteraciones.Location = new System.Drawing.Point(128, 28);
            this.txtIteraciones.Margin = new System.Windows.Forms.Padding(4);
            this.txtIteraciones.Name = "txtIteraciones";
            this.txtIteraciones.Size = new System.Drawing.Size(61, 21);
            this.txtIteraciones.TabIndex = 9;
            this.txtIteraciones.Text = "1000";
            // 
            // lblIteraciones
            // 
            this.lblIteraciones.AutoSize = true;
            this.lblIteraciones.Location = new System.Drawing.Point(44, 31);
            this.lblIteraciones.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIteraciones.Name = "lblIteraciones";
            this.lblIteraciones.Size = new System.Drawing.Size(76, 14);
            this.lblIteraciones.TabIndex = 8;
            this.lblIteraciones.Text = "Iteraciones:";
            // 
            // txtMostrarHasta
            // 
            this.txtMostrarHasta.Location = new System.Drawing.Point(128, 87);
            this.txtMostrarHasta.Margin = new System.Windows.Forms.Padding(4);
            this.txtMostrarHasta.Name = "txtMostrarHasta";
            this.txtMostrarHasta.Size = new System.Drawing.Size(61, 21);
            this.txtMostrarHasta.TabIndex = 7;
            this.txtMostrarHasta.Text = "100";
            // 
            // lblMostrarHasta
            // 
            this.lblMostrarHasta.AutoSize = true;
            this.lblMostrarHasta.Location = new System.Drawing.Point(29, 90);
            this.lblMostrarHasta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMostrarHasta.Name = "lblMostrarHasta";
            this.lblMostrarHasta.Size = new System.Drawing.Size(91, 14);
            this.lblMostrarHasta.TabIndex = 6;
            this.lblMostrarHasta.Text = "Mostrar hasta:";
            // 
            // txtMostrarDesde
            // 
            this.txtMostrarDesde.Location = new System.Drawing.Point(128, 58);
            this.txtMostrarDesde.Margin = new System.Windows.Forms.Padding(4);
            this.txtMostrarDesde.Name = "txtMostrarDesde";
            this.txtMostrarDesde.Size = new System.Drawing.Size(61, 21);
            this.txtMostrarDesde.TabIndex = 1;
            this.txtMostrarDesde.Text = "0";
            // 
            // lblMostrarDesde
            // 
            this.lblMostrarDesde.AutoSize = true;
            this.lblMostrarDesde.Location = new System.Drawing.Point(24, 61);
            this.lblMostrarDesde.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMostrarDesde.Name = "lblMostrarDesde";
            this.lblMostrarDesde.Size = new System.Drawing.Size(96, 14);
            this.lblMostrarDesde.TabIndex = 0;
            this.lblMostrarDesde.Text = "Mostrar desde:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Media (min):";
            // 
            // groupSalida
            // 
            this.groupSalida.Controls.Add(this.txtResultados);
            this.groupSalida.Location = new System.Drawing.Point(472, 26);
            this.groupSalida.Margin = new System.Windows.Forms.Padding(4);
            this.groupSalida.Name = "groupSalida";
            this.groupSalida.Padding = new System.Windows.Forms.Padding(4);
            this.groupSalida.Size = new System.Drawing.Size(671, 181);
            this.groupSalida.TabIndex = 3;
            this.groupSalida.TabStop = false;
            this.groupSalida.Text = "Salidas";
            // 
            // txtResultados
            // 
            this.txtResultados.BackColor = System.Drawing.Color.Silver;
            this.txtResultados.Location = new System.Drawing.Point(8, 22);
            this.txtResultados.Margin = new System.Windows.Forms.Padding(4);
            this.txtResultados.Multiline = true;
            this.txtResultados.Name = "txtResultados";
            this.txtResultados.ReadOnly = true;
            this.txtResultados.Size = new System.Drawing.Size(655, 156);
            this.txtResultados.TabIndex = 10;
            // 
            // gridView
            // 
            this.gridView.AllowUserToAddRows = false;
            this.gridView.AllowUserToDeleteRows = false;
            this.gridView.AllowUserToResizeColumns = false;
            this.gridView.AllowUserToResizeRows = false;
            this.gridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.gridView.ColumnHeadersHeight = 65;
            this.gridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIteracion,
            this.colReloj,
            this.colEvento,
            this.colRndLlegadaReloj,
            this.colTiempoEntreLlegadaReloj,
            this.colProximaLlegadaReloj,
            this.colRndFinControl,
            this.colTControl,
            this.colTiempoAtencion,
            this.colTiempoAtencion2,
            this.colTiempoAtencion3,
            this.colCola,
            this.Empleado_1,
            this.colEmpleado1HoraInicioOcup,
            this.acumEmpleado1Ocupacion,
            this.Empleado_2,
            this.colEmpleado2HoraInicioOcup,
            this.acumEmpleado2Ocupacion,
            this.Empleado_3,
            this.colEmpleado3HoraInicioOcup,
            this.acumEmpleado3Ocupacion,
            this.cantClientesLlegan,
            this.cantRelojesControlados,
            this.cantClientesConEsperaFinalizada,
            this.cantClientesAceptados,
            this.acuTiempoEsperaReloj});
            this.gridView.EnableHeadersVisualStyles = false;
            this.gridView.Location = new System.Drawing.Point(16, 214);
            this.gridView.MultiSelect = false;
            this.gridView.Name = "gridView";
            this.gridView.ReadOnly = true;
            this.gridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.gridView.RowHeadersVisible = false;
            this.gridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridView.Size = new System.Drawing.Size(1128, 368);
            this.gridView.TabIndex = 4;
            // 
            // txtMediaLlegadaReloj
            // 
            this.txtMediaLlegadaReloj.Location = new System.Drawing.Point(145, 31);
            this.txtMediaLlegadaReloj.Margin = new System.Windows.Forms.Padding(4);
            this.txtMediaLlegadaReloj.Name = "txtMediaLlegadaReloj";
            this.txtMediaLlegadaReloj.Size = new System.Drawing.Size(61, 21);
            this.txtMediaLlegadaReloj.TabIndex = 11;
            this.txtMediaLlegadaReloj.Text = "0,6";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtMediaLlegadaReloj);
            this.groupBox1.Location = new System.Drawing.Point(16, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(227, 76);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Llegada Reloj";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 14);
            this.label3.TabIndex = 12;
            this.label3.Text = "Exponencial negativa";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtControlDesviacion);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtControlMedia);
            this.groupBox2.Location = new System.Drawing.Point(16, 106);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(227, 101);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Control Reloj";
            // 
            // txtControlDesviacion
            // 
            this.txtControlDesviacion.Location = new System.Drawing.Point(145, 69);
            this.txtControlDesviacion.Margin = new System.Windows.Forms.Padding(4);
            this.txtControlDesviacion.Name = "txtControlDesviacion";
            this.txtControlDesviacion.Size = new System.Drawing.Size(61, 21);
            this.txtControlDesviacion.TabIndex = 14;
            this.txtControlDesviacion.Text = "0,6";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 73);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 14);
            this.label5.TabIndex = 13;
            this.label5.Text = "Desviación (min):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 43);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 14);
            this.label4.TabIndex = 12;
            this.label4.Text = "Media (min):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 14);
            this.label2.TabIndex = 10;
            this.label2.Text = "Distribución Normal";
            // 
            // txtControlMedia
            // 
            this.txtControlMedia.Location = new System.Drawing.Point(145, 40);
            this.txtControlMedia.Margin = new System.Windows.Forms.Padding(4);
            this.txtControlMedia.Name = "txtControlMedia";
            this.txtControlMedia.Size = new System.Drawing.Size(61, 21);
            this.txtControlMedia.TabIndex = 11;
            this.txtControlMedia.Text = "4,8";
            // 
            // btnEnunciado
            // 
            this.btnEnunciado.Location = new System.Drawing.Point(1032, 12);
            this.btnEnunciado.Name = "btnEnunciado";
            this.btnEnunciado.Size = new System.Drawing.Size(111, 20);
            this.btnEnunciado.TabIndex = 15;
            this.btnEnunciado.Text = "Enunciado";
            this.btnEnunciado.UseVisualStyleBackColor = false;
            this.btnEnunciado.Click += new System.EventHandler(this.btnEnunciado_Click);
            // 
            // colIteracion
            // 
            this.colIteracion.FillWeight = 50F;
            this.colIteracion.Frozen = true;
            this.colIteracion.HeaderText = "Iter";
            this.colIteracion.Name = "colIteracion";
            this.colIteracion.ReadOnly = true;
            this.colIteracion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colIteracion.Width = 40;
            // 
            // colReloj
            // 
            this.colReloj.Frozen = true;
            this.colReloj.HeaderText = "Reloj";
            this.colReloj.Name = "colReloj";
            this.colReloj.ReadOnly = true;
            this.colReloj.Width = 50;
            // 
            // colEvento
            // 
            this.colEvento.Frozen = true;
            this.colEvento.HeaderText = "Evento";
            this.colEvento.Name = "colEvento";
            this.colEvento.ReadOnly = true;
            this.colEvento.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colEvento.Width = 150;
            // 
            // colRndLlegadaReloj
            // 
            this.colRndLlegadaReloj.HeaderText = "RND";
            this.colRndLlegadaReloj.Name = "colRndLlegadaReloj";
            this.colRndLlegadaReloj.ReadOnly = true;
            this.colRndLlegadaReloj.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colRndLlegadaReloj.Width = 40;
            // 
            // colTiempoEntreLlegadaReloj
            // 
            this.colTiempoEntreLlegadaReloj.HeaderText = "T Entre Llegadas";
            this.colTiempoEntreLlegadaReloj.Name = "colTiempoEntreLlegadaReloj";
            this.colTiempoEntreLlegadaReloj.ReadOnly = true;
            this.colTiempoEntreLlegadaReloj.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colTiempoEntreLlegadaReloj.Width = 65;
            // 
            // colProximaLlegadaReloj
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            this.colProximaLlegadaReloj.DefaultCellStyle = dataGridViewCellStyle1;
            this.colProximaLlegadaReloj.HeaderText = "T Prox Llegada";
            this.colProximaLlegadaReloj.Name = "colProximaLlegadaReloj";
            this.colProximaLlegadaReloj.ReadOnly = true;
            this.colProximaLlegadaReloj.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colProximaLlegadaReloj.Width = 65;
            // 
            // colRndFinControl
            // 
            this.colRndFinControl.HeaderText = "RND";
            this.colRndFinControl.Name = "colRndFinControl";
            this.colRndFinControl.ReadOnly = true;
            this.colRndFinControl.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colRndFinControl.Width = 40;
            // 
            // colTControl
            // 
            this.colTControl.HeaderText = "T Control";
            this.colTControl.Name = "colTControl";
            this.colTControl.ReadOnly = true;
            this.colTControl.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colTControl.Width = 65;
            // 
            // colTiempoAtencion
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            this.colTiempoAtencion.DefaultCellStyle = dataGridViewCellStyle2;
            this.colTiempoAtencion.HeaderText = "Fin Control 1";
            this.colTiempoAtencion.Name = "colTiempoAtencion";
            this.colTiempoAtencion.ReadOnly = true;
            this.colTiempoAtencion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colTiempoAtencion.Width = 65;
            // 
            // colTiempoAtencion2
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver;
            this.colTiempoAtencion2.DefaultCellStyle = dataGridViewCellStyle3;
            this.colTiempoAtencion2.HeaderText = "Fin Control 2";
            this.colTiempoAtencion2.Name = "colTiempoAtencion2";
            this.colTiempoAtencion2.ReadOnly = true;
            this.colTiempoAtencion2.Width = 65;
            // 
            // colTiempoAtencion3
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Silver;
            this.colTiempoAtencion3.DefaultCellStyle = dataGridViewCellStyle4;
            this.colTiempoAtencion3.HeaderText = "Fin Control 3";
            this.colTiempoAtencion3.Name = "colTiempoAtencion3";
            this.colTiempoAtencion3.ReadOnly = true;
            this.colTiempoAtencion3.Width = 65;
            // 
            // colCola
            // 
            this.colCola.HeaderText = "Cola";
            this.colCola.Name = "colCola";
            this.colCola.ReadOnly = true;
            this.colCola.Width = 40;
            // 
            // Empleado_1
            // 
            this.Empleado_1.HeaderText = "Empleado_1";
            this.Empleado_1.Name = "Empleado_1";
            this.Empleado_1.ReadOnly = true;
            // 
            // colEmpleado1HoraInicioOcup
            // 
            this.colEmpleado1HoraInicioOcup.HeaderText = "Empleado1 Hr Inicio Ocup";
            this.colEmpleado1HoraInicioOcup.Name = "colEmpleado1HoraInicioOcup";
            this.colEmpleado1HoraInicioOcup.ReadOnly = true;
            // 
            // acumEmpleado1Ocupacion
            // 
            this.acumEmpleado1Ocupacion.HeaderText = "Empleado_1 Acum Ocup";
            this.acumEmpleado1Ocupacion.Name = "acumEmpleado1Ocupacion";
            this.acumEmpleado1Ocupacion.ReadOnly = true;
            // 
            // Empleado_2
            // 
            this.Empleado_2.HeaderText = "Empleado_2";
            this.Empleado_2.Name = "Empleado_2";
            this.Empleado_2.ReadOnly = true;
            // 
            // colEmpleado2HoraInicioOcup
            // 
            this.colEmpleado2HoraInicioOcup.HeaderText = "Empleado2 Hr Inicio Ocup";
            this.colEmpleado2HoraInicioOcup.Name = "colEmpleado2HoraInicioOcup";
            this.colEmpleado2HoraInicioOcup.ReadOnly = true;
            // 
            // acumEmpleado2Ocupacion
            // 
            this.acumEmpleado2Ocupacion.HeaderText = "Empleado_2 Acum Ocup";
            this.acumEmpleado2Ocupacion.Name = "acumEmpleado2Ocupacion";
            this.acumEmpleado2Ocupacion.ReadOnly = true;
            // 
            // Empleado_3
            // 
            this.Empleado_3.HeaderText = "Empleado_3";
            this.Empleado_3.Name = "Empleado_3";
            this.Empleado_3.ReadOnly = true;
            // 
            // colEmpleado3HoraInicioOcup
            // 
            this.colEmpleado3HoraInicioOcup.HeaderText = "Empleado3 Hr Inicio Ocup";
            this.colEmpleado3HoraInicioOcup.Name = "colEmpleado3HoraInicioOcup";
            this.colEmpleado3HoraInicioOcup.ReadOnly = true;
            // 
            // acumEmpleado3Ocupacion
            // 
            this.acumEmpleado3Ocupacion.HeaderText = "Empleado_3 Acum Ocup";
            this.acumEmpleado3Ocupacion.Name = "acumEmpleado3Ocupacion";
            this.acumEmpleado3Ocupacion.ReadOnly = true;
            // 
            // cantClientesLlegan
            // 
            this.cantClientesLlegan.HeaderText = "Cant Reloj Llegan";
            this.cantClientesLlegan.Name = "cantClientesLlegan";
            this.cantClientesLlegan.ReadOnly = true;
            this.cantClientesLlegan.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cantClientesLlegan.Width = 60;
            // 
            // cantRelojesControlados
            // 
            this.cantRelojesControlados.HeaderText = "Cant Relojes Controlados";
            this.cantRelojesControlados.Name = "cantRelojesControlados";
            this.cantRelojesControlados.ReadOnly = true;
            this.cantRelojesControlados.Width = 60;
            // 
            // cantClientesConEsperaFinalizada
            // 
            this.cantClientesConEsperaFinalizada.HeaderText = "Cant Reloj Espera Finalizada";
            this.cantClientesConEsperaFinalizada.Name = "cantClientesConEsperaFinalizada";
            this.cantClientesConEsperaFinalizada.ReadOnly = true;
            this.cantClientesConEsperaFinalizada.Width = 60;
            // 
            // cantClientesAceptados
            // 
            this.cantClientesAceptados.HeaderText = "Ac Total Reloj";
            this.cantClientesAceptados.Name = "cantClientesAceptados";
            this.cantClientesAceptados.ReadOnly = true;
            this.cantClientesAceptados.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cantClientesAceptados.Width = 60;
            // 
            // acuTiempoEsperaReloj
            // 
            this.acuTiempoEsperaReloj.HeaderText = "Ac. T Espera Reloj";
            this.acuTiempoEsperaReloj.Name = "acuTiempoEsperaReloj";
            this.acuTiempoEsperaReloj.ReadOnly = true;
            this.acuTiempoEsperaReloj.Width = 60;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1156, 604);
            this.Controls.Add(this.btnEnunciado);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gridView);
            this.Controls.Add(this.groupSalida);
            this.Controls.Add(this.groupParamentrosEntrada);
            this.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tienda Relojes";
            this.groupParamentrosEntrada.ResumeLayout(false);
            this.groupParamentrosEntrada.PerformLayout();
            this.groupSalida.ResumeLayout(false);
            this.groupSalida.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnSimular;
        private System.Windows.Forms.GroupBox groupParamentrosEntrada;
        private System.Windows.Forms.TextBox txtMostrarDesde;
        private System.Windows.Forms.Label lblMostrarDesde;
        private System.Windows.Forms.GroupBox groupSalida;
        private System.Windows.Forms.DataGridView gridView;
        private System.Windows.Forms.TextBox txtIteraciones;
        private System.Windows.Forms.Label lblIteraciones;
        private System.Windows.Forms.TextBox txtMostrarHasta;
        private System.Windows.Forms.Label lblMostrarHasta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMediaLlegadaReloj;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtControlDesviacion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtControlMedia;
        private System.Windows.Forms.TextBox txtResultados;
        private System.Windows.Forms.Button btnEnunciado;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIteracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReloj;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEvento;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRndLlegadaReloj;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTiempoEntreLlegadaReloj;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProximaLlegadaReloj;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRndFinControl;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTControl;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTiempoAtencion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTiempoAtencion2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTiempoAtencion3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCola;
        private System.Windows.Forms.DataGridViewTextBoxColumn Empleado_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmpleado1HoraInicioOcup;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumEmpleado1Ocupacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Empleado_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmpleado2HoraInicioOcup;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumEmpleado2Ocupacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Empleado_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmpleado3HoraInicioOcup;
        private System.Windows.Forms.DataGridViewTextBoxColumn acumEmpleado3Ocupacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantClientesLlegan;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantRelojesControlados;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantClientesConEsperaFinalizada;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantClientesAceptados;
        private System.Windows.Forms.DataGridViewTextBoxColumn acuTiempoEsperaReloj;
    }
}

