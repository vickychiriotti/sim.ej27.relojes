﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sim.ej27.proyecto {
    public partial class Form1 : Form {

        private List<Empleado> empleados;
        private Iteracion filaAnterior;
        private Iteracion filaActual;
        private long iteraciones;
        private static Random random;
        private long idUltimoReloj;
        private StringBuilder respuesta;
        private StringBuilder enunciado;
        private decimal porcentajeRelojesAtendidos;
        private int cantidadEmpleados;
        private int iteracionDesde;
        private int iteracionHasta;
        private List<Reloj> cola;
        private double acuTiempoOcupacionE1;
        private double acuTiempoOcupacionE2;
        private double acuTiempoOcupacionE3;
        private double ultimaHora;
        
        public Form1() {
            InitializeComponent();
        }

        private void BtnSimular_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtMostrarDesde.Text) || String.IsNullOrEmpty(txtMostrarHasta.Text) 
                    || String.IsNullOrEmpty(txtIteraciones.Text) || String.IsNullOrEmpty(txtControlDesviacion.Text) 
                    || String.IsNullOrEmpty(txtControlMedia.Text) || String.IsNullOrEmpty(txtMediaLlegadaReloj.Text) )
                {
                    throw new Exception("Debe completar todos los campos.");
                }
                if (Convert.ToInt32(txtMostrarDesde.Text) >= Convert.ToInt32(txtMostrarHasta.Text))
                {
                    throw new Exception("El parametro mostrar desde no puede ser mayor o igual al parametro mostrar hasta.");
                }
                if (Convert.ToInt32(txtIteraciones.Text) <= 0)
                {
                    throw new Exception("El parametro número de iteraciones debe ser mayor a cero.");
                }
                if (Convert.ToInt32(txtMostrarDesde.Text) < 0)
                {
                    throw new Exception("El parametro mostrar desde debe ser mayor o igual a cero.");
                }
                if (Convert.ToInt32(txtMostrarHasta.Text) < 0)
                {
                    throw new Exception("El parametro mostrar hasta debe ser mayor a cero.");
                }
                if (Convert.ToInt32(txtMostrarDesde.Text) > Convert.ToInt32(txtIteraciones.Text))
                {
                    throw new Exception("El parametro mostrar desde no puede ser mayor al número de iteraciones.");
                }
                if (Convert.ToInt32(txtMostrarHasta.Text) > Convert.ToInt32(txtIteraciones.Text))
                {
                    throw new Exception("El parametro mostrar hasta no puede ser mayor al número de iteraciones.");
                }
                if (Convert.ToDouble(txtMediaLlegadaReloj.Text) <= 0)
                {
                    throw new Exception("El parametro media debe ser mayor a cero.");
                }
                if ((Convert.ToInt32(txtMostrarHasta.Text) - Convert.ToInt32(txtMostrarDesde.Text)) > 500)
                {
                    throw new Exception("Mas de 500 iteraciones puede verse comprometida la velocidad de la simulación. Aguarde.");
                }

                acuTiempoOcupacionE1= 0;
                acuTiempoOcupacionE2= 0;
                acuTiempoOcupacionE3= 0;
                idRelojFinControl = 1;
                idUltimoReloj = 0;
                porcentajeRelojesAtendidos = 0;
                cantidadEmpleados = 3;
                respuesta = new StringBuilder();
                txtResultados.Text = null;
                iteracionDesde = Convert.ToInt32(txtMostrarDesde.Text);
                iteracionHasta = Convert.ToInt32(txtMostrarHasta.Text);
                inicializarEmpleadosFilaInicial(cantidadEmpleados);
                Simular();
                eliminarColumnasRelojesSobrantes();
                txtResultados.AppendText(respuesta.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Exclamation,
                                            MessageBoxDefaultButton.Button1);
            }
        }

        private void restablecerColumnasGrilla()
        {
            try
            {
                int index = gridView.Columns["acuTiempoEsperaReloj"].Index + 1;
                while (true)
                {
                    gridView.Columns.RemoveAt(index);
                }
            }
            catch (Exception) { }

            try
            {
                int index = gridView.Columns["acuControlTotalR"].Index + 1;
                while (true)
                {
                    gridView.Columns.RemoveAt(index);
                }
            }
            catch (Exception) { }
        }

        private void eliminarColumnasRelojesSobrantes()
        {
            int index = gridView.Columns["acuTiempoEsperaReloj"].Index + 1;
            int indexColEvento = gridView.Columns["colEvento"].Index;
            int clienteIdLast = 0;
            for (int i = 0; i < gridView.RowCount; i++)
            {
                if (gridView.Rows[i].Cells[indexColEvento].Value.ToString().Contains("Llegada"))
                {
                    String cliente = gridView.Rows[i].Cells[indexColEvento].Value.ToString();
                    clienteIdLast = Convert.ToInt32(cliente.Substring(cliente.LastIndexOf(' ') + 1));
                    break;
                }
            }
            
            int indexDesde = gridView.Columns["acuTiempoEsperaReloj"].Index + 1;
            while (gridView.Columns[index].Name != "Reloj_" + clienteIdLast)
            {
                gridView.Columns.RemoveAt(index);
            }

        }

        private void inicializarEmpleadosFilaInicial(int cantEmpleados)
        {
            random = new Random();
            idUltimoReloj = 0;
            empleados = new List<Empleado>();
            for (int i = 0; i < cantEmpleados; i++)
            {
                Empleado empleado = new Empleado(i+1, Convert.ToDouble(txtControlMedia.Text), Convert.ToDouble(txtControlDesviacion.Text));
                empleados.Add(empleado);
            }
            filaActual = new Iteracion(empleados);
            filaAnterior = null;
            iteraciones = Convert.ToInt32(txtIteraciones.Text);
            gridView.Rows.Clear();
            gridView.Refresh();
            filaActual.cola = new List<Reloj>();
            filaActual.reloj = 0;
            filaActual.tiempoEntreLlegadaReloj = generarTiempoEntreLlegadaClientes();
            filaActual.proximaLlegadaReloj = filaActual.reloj + filaActual.tiempoEntreLlegadaReloj;
            filaActual.cantRelojLlegan = 0;
            filaActual.cantRelojesAceptados = 0;
            filaActual.cantRelojesConEsperaFinalizada = 0;
            filaActual.acuTiempoEsperaReloj = 0;
            filaActual.acumTiempoControlTotalReloj = 0;
            filaActual.cantRelojesControlados = 0;
            filaActual.cantRelojLlegan = 0;
        }

        public static double nextRND() {
            int rnd = random.Next(0, 99);
            return (double)rnd / 100;
        }

        private void Simular()
        {
            filaActual.evento = "Inicio";
            insertarFila(filaActual, false);
            long contadorIteraciones = 1;

            while (contadorIteraciones <= iteraciones)
            {
                filaAnterior = (Iteracion)filaActual.Clone();
                filaActual = new Iteracion(contadorIteraciones, filaAnterior);
                Empleado empleado = filaAnterior.obtenerProximoEventoFinAtencion();

                if (empleado != null)
                {
                    if (empleado.horaFinAtencion < filaAnterior.proximaLlegadaReloj)
                    {
                        filaActual.reloj = empleado.horaFinAtencion;
                        eventoFinAtencion(empleado);
                    }
                    else
                    {
                        filaActual.reloj = filaAnterior.proximaLlegadaReloj;
                        eventoLlegadaCliente();
                    }
                }
                else
                {
                    filaActual.reloj = filaAnterior.proximaLlegadaReloj;
                    eventoLlegadaCliente();
                }

                if (contadorIteraciones >= iteracionDesde && contadorIteraciones <= iteracionHasta)
                {
                    insertarFila(filaActual, true);
                }

                filaActual.Relojes.TrimExcess();
                contadorIteraciones++;
            }

            if (iteracionHasta <= iteraciones)
            {
                Iteracion vectorFinalSimulacion = VectorFinSimulacion();
                insertarFila(vectorFinalSimulacion, false);
            }
            ultimaHora = filaActual.reloj;
            escribirResultados(filaAnterior);
        }

        public Reloj extraerPrimeroCola(Iteracion filaActual)
        {
            Reloj reloj = cola.First();
            this.cola.RemoveAt(0);
            this.cola.TrimExcess();
            return reloj;
        }

        private void insertarFila(Iteracion fila, bool insertarRelojes)
        {
            int index = gridView.Rows.Add();
            gridView.Rows[index].Cells[colIteracion.Index].Value = fila.numero;
            gridView.Rows[index].Cells[colEvento.Index].Value = fila.evento;
            gridView.Rows[index].Cells[colReloj.Index].Value = fila.reloj;
            gridView.Rows[index].Cells[colRndLlegadaReloj.Index].Value = fila.rndLlegadaReloj;
            gridView.Rows[index].Cells[colTiempoEntreLlegadaReloj.Index].Value = fila.tiempoEntreLlegadaReloj;
            gridView.Rows[index].Cells[colProximaLlegadaReloj.Index].Value = fila.proximaLlegadaReloj;
            gridView.Rows[index].Cells[colRndFinControl.Index].Value = fila.rndFinControl;
            gridView.Rows[index].Cells[colTControl.Index].Value = fila.tiempoControl;
            gridView.Rows[index].Cells[cantClientesLlegan.Index].Value = fila.cantRelojLlegan;
            gridView.Rows[index].Cells[cantClientesAceptados.Index].Value = fila.acumTiempoControlTotalReloj;//fila.cantRelojesAceptados;
            gridView.Rows[index].Cells[cantClientesConEsperaFinalizada.Index].Value = fila.cantRelojesConEsperaFinalizada;
            gridView.Rows[index].Cells[cantRelojesControlados.Index].Value = fila.cantRelojesControlados;
            gridView.Rows[index].Cells[acuTiempoEsperaReloj.Index].Value = fila.acuTiempoEsperaReloj;
            
            if (fila.cola != null)
            {
                gridView.Rows[index].Cells[colCola.Index].Value = fila.cola.Count();
            }
            else
            {
                gridView.Rows[index].Cells[colCola.Index].Value = 0;
            }

           foreach (Empleado empleado in fila.Empleados)
            {
                if (insertarRelojes)
                {
                    foreach (Reloj reloj in fila.Relojes)
                    {
                        if (!gridView.Columns.Contains("Reloj_" + reloj.id.ToString()))
                        {
                            DataGridViewColumn column = new DataGridViewColumn();
                            column.CellTemplate = new DataGridViewTextBoxCell();
                            column.Name = "Reloj_" + reloj.id.ToString();
                            column.HeaderText = "Reloj " + reloj.id.ToString();
                            column.Width = 60;
                            gridView.Columns.Insert(gridView.ColumnCount, column);

                            column = new DataGridViewColumn();
                            column.CellTemplate = new DataGridViewTextBoxCell();
                            column.Name = "Reloj_" + reloj.id.ToString() + "_inicioEspera";
                            column.HeaderText = "T.I Esp.Cli. " + reloj.id.ToString();
                            column.Width = 60;
                            gridView.Columns.Insert(gridView.ColumnCount, column);
                            
                            DataGridViewColumn column2 = new DataGridViewColumn();
                            column2.CellTemplate = new DataGridViewTextBoxCell();
                            column2.Name = "Reloj_" + reloj.id.ToString() + "_Llegada";
                            column2.HeaderText = "T llegada " + reloj.id.ToString();
                            column2.Width = 60;
                            gridView.Columns.Insert(gridView.ColumnCount, column2);
                        }
                        gridView.Rows[index].Cells["Reloj_" + reloj.id.ToString()].Value = reloj.estadoGrilla();
                        gridView.Rows[index].Cells["Reloj_" + reloj.id.ToString() + "_inicioEspera"].Value = reloj.relojInicioEspera;
                        gridView.Rows[index].Cells["Reloj_" + reloj.id.ToString() + "_Llegada"].Value = reloj.relojLlegada;
                    }
                }
                if (empleado.nombre == "Empleado_1")
                {
                    gridView.Rows[index].Cells[Empleado_1.Index].Value = empleado.estado;
                    gridView.Rows[index].Cells[colEmpleado1HoraInicioOcup.Index].Value = empleado.inicioTiempoOcupacion;
                    gridView.Rows[index].Cells[acumEmpleado1Ocupacion.Index].Value = empleado.acuTiempoOcupacion; //filaActual.acumTiempoInicioEmpleadoOcupado
                    gridView.Rows[index].Cells[colTiempoAtencion.Index].Value = empleado.horaFinAtencion;
                    acuTiempoOcupacionE1 = empleado.acuTiempoOcupacion;
                }
                if (empleado.nombre == "Empleado_2")
                {
                    gridView.Rows[index].Cells[Empleado_2.Index].Value = empleado.estado;
                    gridView.Rows[index].Cells[colEmpleado2HoraInicioOcup.Index].Value = empleado.inicioTiempoOcupacion;
                    gridView.Rows[index].Cells[acumEmpleado2Ocupacion.Index].Value = empleado.acuTiempoOcupacion;
                    gridView.Rows[index].Cells[colTiempoAtencion2.Index].Value = empleado.horaFinAtencion;
                    acuTiempoOcupacionE2 = empleado.acuTiempoOcupacion;
                }
                if (empleado.nombre == "Empleado_3")
                {
                    gridView.Rows[index].Cells[Empleado_3.Index].Value = empleado.estado;
                    gridView.Rows[index].Cells[colEmpleado3HoraInicioOcup.Index].Value = empleado.inicioTiempoOcupacion;
                    gridView.Rows[index].Cells[acumEmpleado3Ocupacion.Index].Value = empleado.acuTiempoOcupacion;
                    gridView.Rows[index].Cells[colTiempoAtencion3.Index].Value = empleado.horaFinAtencion;
                    acuTiempoOcupacionE3 = empleado.acuTiempoOcupacion;
                }
           }
        }
        private long idRelojFinControl=1;
        
        private void eventoFinAtencion(Empleado empleado)
        {
            Reloj relojFinAten = filaActual.Relojes.Find(item => item.id == empleado.relojActual.id);
            filaActual.evento = "Fin Aten Emp" + empleado.id + " /Reloj:"+ relojFinAten.id;
            filaActual.Relojes.RemoveAll(item => item.Equals(relojFinAten));
            filaActual.Relojes.TrimExcess();
          
            if (filaActual.cola.Count != 0)
            {
                //Hay elementos en la cola
                Reloj reloj = filaActual.extraerPrimeroCola(filaActual);
                reloj.estado = Reloj.SIENDO_ATENDIDO;
                filaActual.cantRelojesConEsperaFinalizada++;
                filaActual.acuTiempoEsperaReloj = (filaActual.acuTiempoEsperaReloj + filaActual.reloj - reloj.relojLlegada);
                reloj.empleado = empleado.id;
                empleado.relojActual = reloj;

                empleado.generarFinAtencion(filaActual);
              
                if (reloj.relojInicioEspera != 0 && empleado.estado == Empleado.ESTADO_LIBRE)
                {
                    empleado.inicioTiempoOcupacion = filaActual.reloj;
                                                                    
                }
                empleado.estado = Empleado.ESTADO_OCUPADO;
            }
            else
            {
                empleado.generarFinAtencion(filaActual);
                empleado.estado = Empleado.ESTADO_LIBRE;
                empleado.calcularAcumTiempoOcupacion(filaAnterior, filaActual, empleado.id);
            }
            Empleado empladoFAnterior = filaAnterior.Empleados.Find(item => item.id == empleado.id);
            filaActual.acumTiempoControlTotalReloj += filaActual.reloj - relojFinAten.relojInicioEspera;
            filaActual.cantRelojesControlados++;
        }
        
        private Reloj eventoLlegadaCliente()
        {
            idUltimoReloj++;
            Reloj reloj = new Reloj(idUltimoReloj, filaActual.reloj);
            filaActual.cantRelojLlegan++;
            filaActual.evento = "Llegada Reloj " + reloj.id;
            filaActual.tiempoEntreLlegadaReloj = generarTiempoEntreLlegadaClientes();
            filaActual.proximaLlegadaReloj = filaActual.reloj + filaActual.tiempoEntreLlegadaReloj;
            reloj.relojLlegada = filaActual.reloj;
            Empleado empleado = buscarEmpleadoLibre();
            
            if (empleado != null)
            {
                reloj.estado = Reloj.SIENDO_ATENDIDO;
                reloj.empleado = empleado.id;
                empleado.relojActual = reloj;
                empleado.generarFinAtencion(filaActual);
                empleado.inicioTiempoOcupacion = filaActual.reloj;
                empleado.acuTiempoOcupacion = Math.Round(empleado.acuTiempoOcupacion + filaActual.reloj - empleado.inicioTiempoOcupacion, 2);
                filaActual.cantRelojesConEsperaFinalizada++;
                filaActual.acuTiempoEsperaReloj = (filaActual.acuTiempoEsperaReloj + filaActual.reloj - reloj.relojLlegada);
                
                if (reloj.relojInicioEspera != 0)
                {
                    filaActual.acumTiempoInicioEmpleadoOcupado = filaActual.acumTiempoInicioEmpleadoOcupado + (filaActual.reloj - reloj.relojInicioEspera);
                }
            }
            else
            {
                filaActual.cola.Add(reloj);
                reloj.relojInicioEspera = filaActual.reloj;
                reloj.estado = Reloj.ESPERANDO_ATENCION;
            }

            filaActual.Relojes.Add(reloj);
            return reloj;
        }

        private double generarTiempoEntreLlegadaClientes()
        {
            filaActual.rndLlegadaReloj = nextRND();
            if (filaActual.rndLlegadaReloj == 0)
            { filaActual.rndLlegadaReloj = nextRND(); }
            double tiempoEntreLlegadaCliente = (Convert.ToDouble(txtMediaLlegadaReloj.Text) * -1) * Math.Log(1 - filaActual.rndLlegadaReloj);
            return Math.Round(tiempoEntreLlegadaCliente, 2);
        }

        private Empleado buscarEmpleadoLibre()
        {
            return empleados.Find(item => item.estado.Equals(Empleado.ESTADO_LIBRE));
        }

        public Empleado obtenerEmpleadoFinAtencion(Iteracion iter)
        {
            Empleado empleado = null;
            List<Empleado> empleadosOcupados = filaActual.Empleados.FindAll(item => item.estado == Empleado.ESTADO_OCUPADO);
            for (int i = 0; i < empleadosOcupados.Count; i++)
            {
                if (empleadosOcupados.ElementAt(i).horaFinAtencion <= iter.tiempoFinControl)
                    empleado = empleadosOcupados.ElementAt(i);
            }
            return empleado;
        }

        private Iteracion VectorFinSimulacion()
        {
            filaAnterior = (Iteracion)filaActual.Clone();
            Iteracion vectorFinSimulacion = new Iteracion(long.Parse(txtIteraciones.Text), filaAnterior);
            vectorFinSimulacion.reloj = filaAnterior.reloj ;
            vectorFinSimulacion.evento = "Fin Simulacion";
            return vectorFinSimulacion;
        }

        private void escribirResultados(Iteracion fila)
        {
            /*
             1) Tiempo medio que debe esperar un reloj para ser controlado
             2) Tiempo total promedio de un reloj en el sistema desde que entra a ser controlado hasta que sale ok o fallado
             3) Porcentaje de utilizacion de los trabajadores
             */
            respuesta.Append("* El total de Relojes que ingresan al sistema es de: " + (filaActual.cantRelojLlegan));
            respuesta.AppendLine();
            respuesta.Append("* Relojes controlados: " + filaActual.cantRelojesControlados);
            respuesta.AppendLine();
            respuesta.Append("a) El tiempo de espera promedio de los relojes es de: " + Math.Round(filaActual.acuTiempoEsperaReloj / filaActual.cantRelojesConEsperaFinalizada, 2) + " minutos");
            respuesta.AppendLine();
            respuesta.Append("b) Tiempo total promedio de un reloj desde que entra al sistema a ser controlado hasta que sale: " + Math.Round(filaActual.acumTiempoControlTotalReloj / filaActual.cantRelojesControlados, 2) + " minutos");
            respuesta.AppendLine();
            porcentajeRelojesAtendidos = Convert.ToDecimal(Convert.ToDecimal(filaActual.cantRelojesControlados / filaActual.cantRelojLlegan));
            porcentajeRelojesAtendidos = Math.Round(porcentajeRelojesAtendidos * 100, 2);
            respuesta.Append("c) Porcentaje de utilizacion de los trabajadores: " );
            respuesta.AppendLine();

            if (acuTiempoOcupacionE1 != 0)
            {
                acuTiempoOcupacionE1 = Math.Round(acuTiempoOcupacionE1 / ultimaHora * 100, 2);
                respuesta.Append("     - Empleado 1: " + acuTiempoOcupacionE1 + " %");
            }
            else
            {
                Empleado empleado1 = filaActual.Empleados.Find(item => item.id == 1);
                acuTiempoOcupacionE1 = Math.Round(((ultimaHora - empleado1.inicioTiempoOcupacion) / ultimaHora) * 100, 2);
                respuesta.Append("     - Empleado 1: " + acuTiempoOcupacionE1 + "%");
            }
            respuesta.AppendLine();
            if (acuTiempoOcupacionE2 != 0)
            {
                acuTiempoOcupacionE2 = Math.Round(acuTiempoOcupacionE2 / ultimaHora * 100, 2); //porcentaje
                respuesta.Append("     - Empleado 2: " + acuTiempoOcupacionE2 + " %");
            }
            else
            {
                Empleado empleado2 = filaActual.Empleados.Find(item => item.id == 2);
                acuTiempoOcupacionE2 = Math.Round(((ultimaHora - empleado2.inicioTiempoOcupacion) / ultimaHora) * 100, 2);
                respuesta.Append("     - Empleado 2: " + acuTiempoOcupacionE2 + "%");
            }
            respuesta.AppendLine();

            if (acuTiempoOcupacionE3 != 0)
            {
                acuTiempoOcupacionE3 = Math.Round(acuTiempoOcupacionE3 / ultimaHora * 100, 2);
                respuesta.Append("     - Empleado 3: " + acuTiempoOcupacionE3  + " %");
            }
            else
            {
                Empleado empleado3 = filaActual.Empleados.Find(item => item.id == 3);
                acuTiempoOcupacionE3 = Math.Round(((ultimaHora - empleado3.inicioTiempoOcupacion) /ultimaHora) * 100, 2);
                respuesta.Append("     - Empleado 3: "  + acuTiempoOcupacionE3 + "%");
            }
            respuesta.AppendLine();

            respuesta.Append("     - Promedio 3 empleados: " + Math.Round(((acuTiempoOcupacionE3 + acuTiempoOcupacionE1 + acuTiempoOcupacionE2)/3) , 2) + " %");
        }

        private void btnEnunciado_Click(object sender, EventArgs e)
        {
            enunciado = new StringBuilder();
            enunciado.Append("En un sistema de control de calidad de una fábrica de relojes en el que trabajan tres personas que");
            enunciado.AppendLine();
            enunciado.Append("controlan relojes, el tiempo de llegada de cada reloj tiene una distribución exponencial, con un");
            enunciado.AppendLine();
            enunciado.Append("tiempo entre llegadas de 0,01 horas (media). Los relojes se controlan de uno en uno, a medida que ");
            enunciado.AppendLine();
            enunciado.Append("van llegando. El tiempo necesario para controlar un reloj tiene una distribución normal, con una");
            enunciado.AppendLine();
            enunciado.Append("media de 0,08 horas y una desviación estándar de 0,01 horas, determinar:");
            enunciado.AppendLine();
            enunciado.Append("a) El tiempo medio que debe esperar un reloj antes de ser controlado");
            enunciado.AppendLine();
            enunciado.Append("b) El tiempo total promedio de un reloj en el sistema, desde que entra para ser controlado hasta");
            enunciado.AppendLine();
            enunciado.Append("que sale Ok o fallado.");
            enunciado.AppendLine();
            enunciado.Append("c) El porcentaje de utilización de cada uno de los operarios que controlan a los relojes.");

            MessageBox.Show(enunciado.ToString(), "Enunciado");
        }

        

        
    }

       
 }
