﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sim.ej27.proyecto {
    public class Reloj {
        public long id { get; set; }
        public string estado { get; set; }
        public Double relojInicioEspera { get; set; }
        public long empleado { get; set; }
        public Double relojLlegada { get; set; }

        public const String ESPERANDO_ATENCION = "E.A.";
        public const String SIENDO_ATENDIDO = "S.A.";
       
        public Reloj(long id, double reloj)
        {
            this.id = id;
            this.estado = ESPERANDO_ATENCION;
        }

        public string estadoGrilla()
        {
            return (estado == SIENDO_ATENDIDO) ? SIENDO_ATENDIDO + "(" + empleado + ")" : estado;
        }
    }
}
