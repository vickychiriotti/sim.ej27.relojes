﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sim.ej27.proyecto
{
    public class Empleado
    {
        public const String ESTADO_LIBRE = "Libre";
        public const String ESTADO_OCUPADO = "Ocupado";
        public long id { get; set; }
        public string nombre { get; set; }
        public string estado { get; set; }
        public double inicioTiempoOcupacion { get; set; }
        public double acuTiempoOcupacion { get; set; }
        public double horaFinAtencion { get; set; }
        public Reloj relojActual { get; set; }
        private Double valorMedia;
        private Double valorDesviacion;

        public Empleado(int id) {
            this.id = id;
            this.nombre = "Empleado_" + id;
            this.estado = ESTADO_LIBRE;
            this.horaFinAtencion = 0;
            this.inicioTiempoOcupacion = 0;
            this.acuTiempoOcupacion = 0;
        }

        public void generarFinAtencion(Iteracion filaActual)
        {
            double rnd = Form1.nextRND();
            double tiempoControl = (rnd * valorDesviacion + valorMedia);
            filaActual.rndFinControl = Math.Round(rnd, 2);
            filaActual.tiempoControl = Math.Round(tiempoControl, 2);
            this.horaFinAtencion = Math.Round(filaActual.reloj + filaActual.tiempoControl, 2);
            this.estado = ESTADO_OCUPADO;
        }

        public Empleado(int id, Double valorMedia, Double valorDesviacion)
        {
            this.id = id;
            this.nombre = "Empleado_" + id;
            this.estado = ESTADO_LIBRE;
            this.horaFinAtencion = 0;
            this.acuTiempoOcupacion = 0;
            this.inicioTiempoOcupacion = 0;
            this.valorMedia = valorMedia;
            this.valorDesviacion = valorDesviacion;
        }

        public void calcularHoraInicioOcupacion(Iteracion filaAnterior, Iteracion filaActual, long empleadoId)
        {
            Empleado empladoFAnterior = filaAnterior.Empleados.Find(item => item.id == empleadoId);
            Empleado empladoActual = filaActual.Empleados.Find(item => item.id == empleadoId);

            if (empladoActual.estado == Empleado.ESTADO_OCUPADO && empladoFAnterior.estado == Empleado.ESTADO_OCUPADO)
            {
                    this.inicioTiempoOcupacion = filaActual.reloj;
            }
        }

        public void calcularAcumTiempoOcupacion(Iteracion filaAnterior, Iteracion filaActual, long empleadoId)
        {
            Empleado empladoFAnterior = filaAnterior.Empleados.Find(item => item.id == empleadoId);
            Empleado empladoActual = filaActual.Empleados.Find(item => item.id == empleadoId);
            if (empladoActual.estado == Empleado.ESTADO_LIBRE && empladoFAnterior.estado == Empleado.ESTADO_LIBRE)
            {
                empladoActual.acuTiempoOcupacion += Math.Round(filaActual.reloj - empladoActual.inicioTiempoOcupacion, 2);
            }
        }

    }
}
