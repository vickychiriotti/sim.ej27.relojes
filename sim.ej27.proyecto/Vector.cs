﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sim.ej27.proyecto
{
    public class Vector
    {
        private double tReloj;
        private int evento;//0 Inicio - 1 Llegada - 2 Fin

        //LLEGADA
        //Tiempo Entre Llegadas=-0,6*(RND-1)
        private double RNDLlegada;
        private double tEntreLlegada;
        private double tProximaLlegada;

        //FIN CONTROL
        private double RNDFinControl;
        private double tControl;
        private double tFinControl;

        //COLA
        private int cola;
        private int cantRelojes;

        //EMPLEADOS
        private int estadoEmpleado1; //0 LIBRE - 1 OCUPADO
        private double horaInicioOcupacionEmpleado1;
        private int estadoEmpleado2; //0 LIBRE - 1 OCUPADO
        private double horaInicioOcupacionEmpleado2;
        private int estadoEmpleado3;//0 LIBRE - 1 OCUPADO
        private double horaInicioOcupacionEmpleado3;
        private double tOcupacion;

        private List<Reloj> relojes;

        //Vector Inicial
        public Vector() {
            Random random = new Random();
            tReloj = 0;
            evento = 0;
            RNDLlegada = random.NextDouble();
            tEntreLlegada = (-0.6) * (Math.Log(RNDLlegada - 1));//=-0,6*(RND-1)
            tProximaLlegada = tReloj + tEntreLlegada;
            cola = 0;
        }

        //Constructor LLEGADA
        public Vector(double tReloj, int evento) {
            Random random = new Random();
            this.tReloj = tReloj;
            this.evento = evento;
            RNDLlegada = random.NextDouble();
            this.tEntreLlegada = (-0.6) * (Math.Log(RNDLlegada - 1));//=-0,6*(RND-1)
            this.tProximaLlegada = tReloj + tEntreLlegada;
            cola = 0;
            relojes = new List<Reloj>();
        }

        //Constructor FIN
        public Vector(double tReloj, int evento, double tProximaLlegada) {
            Random random = new Random();
            this.tReloj = tReloj;
            this.evento = evento;
            this.tProximaLlegada = tProximaLlegada; // es el mismo del vector anterior

            RNDFinControl = random.NextDouble();
            this.tControl= RNDFinControl * 0.6 + 4.8;
            tFinControl = tReloj + tControl;
        }
        
        public void SetRelojes(Reloj reloj) {
            relojes.Add(reloj);
        }

        public String[] GetVector() {
            String[] vector = new String[5];
            vector[0] = tReloj.ToString();
            vector[1] = evento.ToString();
            vector[2] = RNDLlegada.ToString();
            vector[3] = tEntreLlegada.ToString();
            vector[4] = tProximaLlegada.ToString();
            return vector;
        }

        public void SetCola(int cola) {
            this.cola = cola;
        }
        public void SetEstadoEmpleado1(int estadoEmpleado1) {
            this.estadoEmpleado1 = estadoEmpleado1;
        }
        public void SetEstadoEmpleado2(int estadoEmpleado2) {
            this.estadoEmpleado2 = estadoEmpleado2;
        }
        public void SetEstadoEmpleado3(int SetEstadoEmpleado3) {
            this.estadoEmpleado3= SetEstadoEmpleado3;
        }
        public void SetcantRelojes(int cola) {
            this.cola = cantRelojes;
        }

        public int GetEvento() {
            return evento;
        }
        public double GetReloj()
        {
            return tReloj;
        }
        public double GetProximaLlegada() {
            return tProximaLlegada;
        }
        public double GetFinControl() {
            return tFinControl;
        }
        public int GetCola() {
            return cola;
        }
        public int GetEstadoEmpleado1() {
            return estadoEmpleado1;
        }
        public int GetEstadoEmpleado2() {
            return estadoEmpleado2;
        }
        public int GetEstadoEmpleado3() {
            return estadoEmpleado3;
        }
        public int GetcantRelojes() {
            return cantRelojes;
        }
    }
}
